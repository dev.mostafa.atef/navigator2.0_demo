import 'package:nestedroutes/routes/paths/book_route.dart';

class BooksDetailsPath extends BookRoutePath {
  final int id;

  BooksDetailsPath(this.id);
}
